import prefixType from '../prefix-type'
import createResolvableActions from './create-resolvable-actions'
import { DOMAIN_DELIMITER } from '../constants'

function createResolvableActionsDomain (domain) {
  return prefixType(domain, createResolvableActions, DOMAIN_DELIMITER)
}

export default createResolvableActionsDomain
