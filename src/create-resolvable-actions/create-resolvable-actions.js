import createActions from '../create-actions'
import { withPromise } from './promise'

const defaultHandlers = {
  init: payload => ({ payload }),
  resolve: payload => ({ payload }),
  reject: error => ({ error })
}

function createResolvableActions (type, actionMap = defaultHandlers) {

  if (typeof actionMap === 'function') {
    actionMap = {
      init: actionMap
    }
  } else if (typeof actionMap !== 'object' || Array.isArray(actionMap)) {
    throw new Error('createResolvableActions(type, actionMap): actionMap has to be an Object or Function')
  }
  actionMap = {
    ...defaultHandlers,
    ...actionMap
  }

  const [
    initActionCreator,
    resolveActionCreator,
    rejectActionCreator
  ] = createActions(type, {
    init: actionMap.init,
    resolve: actionMap.resolve,
    reject: actionMap.reject
  })

  const actionCreator = (...args) => {
    let action = initActionCreator(...args)
    const promise = new Promise((resolve, reject) => {
      action.resolve = (...args) => {
        let action = resolveActionCreator(...args)
        setTimeout(() => resolve(action), 0)
        return action
      }
      action.resolve.toString = resolveActionCreator.toString

      action.reject = (...args) => {
        let action = rejectActionCreator(...args)
        setTimeout(() => reject(action), 0)
        return action
      }
      action.reject.toString = rejectActionCreator.toString
    })

    return withPromise(action, promise)
  }
  actionCreator.toString = initActionCreator.toString
  return [
    actionCreator,
    resolveActionCreator,
    rejectActionCreator
  ]
}

export default createResolvableActions
