import createResolvableActions from './create-resolvable-actions'
import { SLICE_DELIMITER } from '../constants'

describe('createResolvableAction', () => {
  describe('default actionMap', () => {
    const actionCreators = createResolvableActions('test')
    const [
      initActionCreator,
      resolveActionCreator,
      rejectActionCreator,
    ] = actionCreators
    it('returns an array of 3 actionCreators', () => {
      expect(Array.isArray(actionCreators)).toBe(true)
      expect(actionCreators.length).toBe(3)
    })

    it('returns an init actionCreator', () => {
      expect(String(initActionCreator)).toBe(`test${SLICE_DELIMITER}init`)
      const action = initActionCreator('value')
      expect(action).toHaveProperty('type', `test${SLICE_DELIMITER}init`)
      expect(action).toHaveProperty('payload', 'value')
    })
    it('returns a resolve actionCreator', () => {
      expect(String(resolveActionCreator)).toBe(`test${SLICE_DELIMITER}resolve`)
      const action = resolveActionCreator('value')
      expect(action).toHaveProperty('type', `test${SLICE_DELIMITER}resolve`)
      expect(action).toHaveProperty('payload', 'value')
    })
    it('returns a reject actionCreator', () => {
      expect(String(rejectActionCreator)).toBe(`test${SLICE_DELIMITER}reject`)
      const action = rejectActionCreator('value')
      expect(action).toHaveProperty('type', `test${SLICE_DELIMITER}reject`)
      expect(action).toHaveProperty('error', 'value')
    })
  })
  describe('function as actionMap', () => {
    const actionCreators = createResolvableActions('test', (a, b) => ({ a, b }))
    const [
      initActionCreator,
      resolveActionCreator,
      rejectActionCreator,
    ] = actionCreators
    it('returns an array of 3 actionCreators', () => {
      expect(Array.isArray(actionCreators)).toBe(true)
      expect(actionCreators.length).toBe(3)
    })

    it('returns an init actionCreator', () => {
      expect(String(initActionCreator)).toBe(`test${SLICE_DELIMITER}init`)
      const action = initActionCreator('value')
      expect(action).toHaveProperty('type', `test${SLICE_DELIMITER}init`)
      expect(action).toHaveProperty('a', 'value')
      expect(action).toHaveProperty('b', undefined)
    })
    it('returns a resolve actionCreator', () => {
      expect(String(resolveActionCreator)).toBe(`test${SLICE_DELIMITER}resolve`)
      const action = resolveActionCreator('value')
      expect(action).toHaveProperty('type', `test${SLICE_DELIMITER}resolve`)
      expect(action).toHaveProperty('payload', 'value')
    })
    it('returns a reject actionCreator', () => {
      expect(String(rejectActionCreator)).toBe(`test${SLICE_DELIMITER}reject`)
      const action = rejectActionCreator('value')
      expect(action).toHaveProperty('type', `test${SLICE_DELIMITER}reject`)
      expect(action).toHaveProperty('error', 'value')
    })
  })

  describe('empty object as actionMap', () => {
    const actionCreators = createResolvableActions('test', {})
    const [
      initActionCreator,
      resolveActionCreator,
      rejectActionCreator,
    ] = actionCreators
    it('returns an array of 3 actionCreators', () => {
      expect(Array.isArray(actionCreators)).toBe(true)
      expect(actionCreators.length).toBe(3)
    })

    it('returns an init actionCreator', () => {
      expect(String(initActionCreator)).toBe(`test${SLICE_DELIMITER}init`)
      const action = initActionCreator('value')
      expect(action).toHaveProperty('type', `test${SLICE_DELIMITER}init`)
      expect(action).toHaveProperty('payload', 'value')
    })
    it('returns a resolve actionCreator', () => {
      expect(String(resolveActionCreator)).toBe(`test${SLICE_DELIMITER}resolve`)
      const action = resolveActionCreator('value')
      expect(action).toHaveProperty('type', `test${SLICE_DELIMITER}resolve`)
      expect(action).toHaveProperty('payload', 'value')
    })
    it('returns a reject actionCreator', () => {
      expect(String(rejectActionCreator)).toBe(`test${SLICE_DELIMITER}reject`)
      const action = rejectActionCreator('value')
      expect(action).toHaveProperty('type', `test${SLICE_DELIMITER}reject`)
      expect(action).toHaveProperty('error', 'value')
    })
  })

  describe('object as actionMap', () => {
    const actionCreators = createResolvableActions('test', {
      init: (a, b) => ({ a, b }),
      resolve: (c, d) => ({ c, d }),
      reject: (e, f) => ({ e, f }),
    })
    const [
      initActionCreator,
      resolveActionCreator,
      rejectActionCreator,
    ] = actionCreators
    it('returns an array of 3 actionCreators', () => {
      expect(Array.isArray(actionCreators)).toBe(true)
      expect(actionCreators.length).toBe(3)
    })

    it('returns an init actionCreator', () => {
      expect(String(initActionCreator)).toBe(`test${SLICE_DELIMITER}init`)
      const action = initActionCreator('val1', 'val2')
      expect(action).toHaveProperty('type', `test${SLICE_DELIMITER}init`)
      expect(action).toHaveProperty('a', 'val1')
      expect(action).toHaveProperty('b', 'val2')
    })
    it('returns a resolve actionCreator', () => {
      expect(String(resolveActionCreator)).toBe(`test${SLICE_DELIMITER}resolve`)
      const action = resolveActionCreator('val1', 'val2')
      expect(action).toHaveProperty('type', `test${SLICE_DELIMITER}resolve`)
      expect(action).toHaveProperty('c', 'val1')
      expect(action).toHaveProperty('d', 'val2')
    })
    it('returns a reject actionCreator', () => {
      expect(String(rejectActionCreator)).toBe(`test${SLICE_DELIMITER}reject`)
      const action = rejectActionCreator('val1', 'val2')
      expect(action).toHaveProperty('type', `test${SLICE_DELIMITER}reject`)
      expect(action).toHaveProperty('e', 'val1')
      expect(action).toHaveProperty('f', 'val2')
    })
  })
  describe('throws an error for unsupported actionMap types', () => {
    expect(() => createResolvableActions('test', [])).toThrow()
  })
})
