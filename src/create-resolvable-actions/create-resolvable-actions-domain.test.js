import createResolvableActionsDomain from './create-resolvable-actions-domain'

describe('createResolvableAction', () => {
  const createResolvableAction = createResolvableActionsDomain('domain')
  it('returns a function', () => {
    expect(typeof createResolvableAction).toBe('function')
  })
  it('executing result will return an array of 3 actionCreators', () => {
    const actionCreators = createResolvableAction('slice')
    expect(Array.isArray(actionCreators)).toBe(true)
    expect(actionCreators.length).toBe(3)
    expect(typeof actionCreators[0]).toBe('function')
    expect(typeof actionCreators[1]).toBe('function')
    expect(typeof actionCreators[2]).toBe('function')
  })
})
