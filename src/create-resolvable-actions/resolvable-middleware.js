import { getPromise } from './promise'

function resolvableMiddleware (store) {
  return function (next) {
    return function (action) {
      if (action !== undefined && action !== null) {
        let promise = getPromise(action)
        if (promise) {
          next(action)
          return promise
        }
      }
      return next(action)
    }
  }
}

export default resolvableMiddleware
