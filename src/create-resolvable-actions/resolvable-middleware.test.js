import resolvableActionsMiddleware from './resolvable-middleware'
import createResolvableActions from './create-resolvable-actions'

import { applyMiddleware, createStore } from 'redux'

describe('resolvableActionsMiddleware', () => {
  const doDispatch = () => {}
  const doGetState = () => {}
  const nextHandler = resolvableActionsMiddleware({ dispatch: doDispatch, getState: doGetState })

  it('must return a function to handle next', () => {
    expect(typeof nextHandler).toBe('function')
    expect(nextHandler.length).toBe(1)
  })

  describe('handle next', () => {
    it('must return a function to handle action', () => {
      const actionHandler = nextHandler()

      expect(typeof actionHandler).toBe('function')
      expect(actionHandler.length).toBe(1)
    })

    describe('handle action', () => {
      it('must return the action when called with a regular action', () => {
        const actionHandler = nextHandler((action) => action)
        const action = { type: 'test' }
        const result = actionHandler(action)
        expect(result).toBe(action)
      })

      it('must return a promise when called with a resolvable action', () => {
        const [actionCreator] = createResolvableActions('type', () => ({}))
        const actionHandler = nextHandler((action) => action)
        const result = actionHandler(actionCreator())
        expect(typeof result.then).toBe('function')
        expect(typeof result.catch).toBe('function')
      })

      it('must resolve to the resolved action-type when the original action is resolved', (done) => {
        const [
          initActionCreator,
          resolveActionCreator
        ] = createResolvableActions('type', () => ({}))

        const actionHandler = nextHandler((action) => action)
        const action = initActionCreator()
        setTimeout(() => action.resolve('test'), 0)
        actionHandler(action).then(result => {
          expect(typeof result).toBe('object')
          expect(result.type).toBe(String(resolveActionCreator))
          expect(result).toEqual({ type: String(resolveActionCreator), payload: 'test' })
          done()
        })
      })
      it('must reject to the rejected action-type when the original action is rejected', (done) => {
        const [
          initActionCreator,
          resolveActionCreator,
          rejectActionCreator
        ] = createResolvableActions('type', () => ({}))

        const actionHandler = nextHandler((action) => action)
        const action = initActionCreator()
        setTimeout(() => action.reject('test'),0)
        actionHandler(action).catch(result => {
          expect(typeof result).toBe('object')
          expect(result.type).toBe(String(rejectActionCreator))
          expect(result).toEqual({ type: String(rejectActionCreator), error: 'test' })
          done()
        })
      })
    })
  })
})
describe('resolvableActionsMiddleware works correctly with redux', () => {
  const store = createStore(
    (state) => state,
    0,
    applyMiddleware(resolvableActionsMiddleware)
  )
  it('must return the action when called with a regular action', () => {
    const action = { type: 'test' }
    const result = store.dispatch(action)
    expect(result).toBe(action)
  })
  it('must return a promise when called with a resolvable action', () => {
    const [actionCreator] = createResolvableActions('type', () => ({}))

    const result = store.dispatch(actionCreator())
    expect(typeof result.then).toBe('function')
    expect(typeof result.catch).toBe('function')
  })
  it('must handle non plain-objects correctly', () => {
    function AwesomeMap() {}
    ;[null, undefined, 42, 'hey', new AwesomeMap()].forEach(nonObject =>
      expect(() => store.dispatch(nonObject)).toThrow(/plain/)
    )
  })
})
