import { getPromise, withPromise, metaKey } from './promise'

describe('getPromise and withPromise', () => {
  it('returns the action with a promise', () => {
    const action = { type: 'action' }
    expect(withPromise(action, 'promise')).toHaveProperty(metaKey, 'promise')
  })
  it('returns the promise from an action', () => {
    const action = { type: 'action', [metaKey]: 'promise' }
    expect(getPromise(action)).toBe('promise')
  })
  it('sets and gets the promise on an action', () => {
    let action = { type: 'action' }
    action = withPromise(action, 'promise')
    expect(action).toHaveProperty(metaKey, 'promise')
    expect(getPromise(action)).toBe('promise')
  })
})
