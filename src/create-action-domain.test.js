import createActionDomain from './create-action-domain'
import { DOMAIN_DELIMITER } from './constants'

describe('createActionDomain()', () => {
  let createAction = createActionDomain()

  it('createActionDomain returns a function', () => {
    expect(typeof createAction).toBe('function')
  })
  it('createAction returns a function', () => {
    let actionCreator = createAction()
    expect(typeof actionCreator).toBe('function')
  })
})
describe('createActionDomain(domain)', () => {
  let domain = 'domain'
  let createAction = createActionDomain(domain)
  let type = 'type'
  describe('createAction(type)', () => {
    it(`returns an actionCreator with toString() => "${domain}${DOMAIN_DELIMITER}${type}"`, () => {
      let actionCreator = createAction(type)
      expect(actionCreator.toString()).toEqual(`${domain}${DOMAIN_DELIMITER}${type}`)
    })

    it(`actionCreator returns an object with type: "${domain}${DOMAIN_DELIMITER}${type}"`, () => {
      let actionCreator = createAction(type)
      expect(actionCreator()).toEqual({ type: `${domain}${DOMAIN_DELIMITER}${type}` })
    })
  })
  describe('createAction(type, (a, b) => ({ a, b }))', () => {
    it(`returns an actionCreator with toString() => "${domain}${DOMAIN_DELIMITER}${type}"`, () => {
      let actionCreator = createAction(type, (a, b) => ({ a, b }))
      expect(actionCreator.toString()).toEqual(`${domain}${DOMAIN_DELIMITER}${type}`)
    })

    it(`actionCreator(input1) returns an object with a: input1, b: undefined`, () => {
      let actionCreator = createAction(type, (a, b) => ({ a, b }))
      let a = 'test2'
      expect(actionCreator(a)).toEqual({ type: `${domain}${DOMAIN_DELIMITER}${type}`, a, b: undefined })
    })
    it(`actionCreator(input1, input2) returns an object with a: input1, b: undefined`, () => {
      let actionCreator = createAction(type, (a, b) => ({ a, b }))
      let a = 'test2'
      let b = 'test3'
      expect(actionCreator(a, b)).not.toEqual({ type: `${domain}${DOMAIN_DELIMITER}${type}`, a, b: undefined })
      expect(actionCreator(a, b)).toEqual({ type: `${domain}${DOMAIN_DELIMITER}${type}`, a, b })
    })
  })

})
