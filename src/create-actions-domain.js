import createActions from './create-actions'
import prefixType from './prefix-type'
import { DOMAIN_DELIMITER } from './constants'

export default (domain) => prefixType(domain, createActions, DOMAIN_DELIMITER)
