import createAction from './create-action'
import prefixType from './prefix-type'

export default (domain) => prefixType(domain, createAction, '//')
