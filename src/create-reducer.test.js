import createReducer from './create-reducer'

describe('createReducer', () => {
  const reducer = createReducer()
  it('returns a function', () => {
    expect(typeof reducer).toBe('function')
  })
  describe('reducer', () => {
    it('returns the initial state (undefined) when none given', () => {
      expect(reducer()).toBe(undefined)
    })
    it('returns the given state when no action matches', () => {
      expect(reducer(1234)).toBe(1234)
    })
    it('has a toString() value of "undefined" when none given', () => {
      expect(String(reducer)).toBe('undefined')
    })
  })
})
describe('createReducer("test")', () => {
  const reducer = createReducer('test')
  it('returns a function', () => {
    expect(typeof reducer).toBe('function')
  })
  describe('reducer', () => {
    it('returns the initial state (undefined) when none given', () => {
      expect(reducer()).toBe(undefined)
    })
    it('returns the given state when no action matches', () => {
      expect(reducer(1234)).toBe(1234)
    })
    it('has a toString() value of "test" when none given', () => {
      expect(String(reducer)).toBe('test')
    })
  })
})
describe('createReducer("test", {})', () => {
  const reducer = createReducer('test', {})
  it('returns a function', () => {
    expect(typeof reducer).toBe('function')
  })
  describe('reducer', () => {
    it('returns the initial state (undefined) when none given', () => {
      expect(reducer()).toBe(undefined)
    })
    it('returns the given state when no action matches', () => {
      expect(reducer(1234)).toBe(1234)
    })
    it('returns the given state when no action matches', () => {
      expect(reducer(1234, { type: 'testing' })).toBe(1234)
    })
    it('has a toString() value of "test" when none given', () => {
      expect(String(reducer)).toBe('test')
    })
  })
})
describe('createReducer("test", {}, "initialState")', () => {
  const reducer = createReducer('test', {}, 'initialState')
  it('returns a function', () => {
    expect(typeof reducer).toBe('function')
  })
  describe('reducer', () => {
    it('returns the initial state ("initialState") when none given', () => {
      expect(reducer()).toBe('initialState')
    })
    it('returns the given state when no action matches', () => {
      expect(reducer(1234)).toBe(1234)
    })
    it('returns the given state when no action matches', () => {
      expect(reducer(1234, { type: 'testing' })).toBe(1234)
    })
    it('has a toString() value of "test" when none given', () => {
      expect(String(reducer)).toBe('test')
    })
  })
})

describe('createReducer("test", actionMap, "initialState")', () => {
  const reducer = createReducer('counter', {
    test: (state) => state,
    add: (state, action) => state + action.payload,
    sub: (state, action) => state - action.payload
  }, 0)
  it('returns a function', () => {
    expect(typeof reducer).toBe('function')
  })
  describe('reducer', () => {
    it('returns the initial state ("initialState") when none given', () => {
      expect(reducer()).toBe(0)
    })
    it('returns the given state when no action matches', () => {
      expect(reducer(1234)).toBe(1234)
    })
    it('returns the given state when no action matches', () => {
      expect(reducer(1234, { type: 'testing' })).toBe(1234)
    })
    it('has a toString() value of "test" when none given', () => {
      expect(String(reducer)).toBe('counter')
    })
    it('correctly handles actions', () => {
      expect(reducer(undefined, { type: 'test' })).toBe(0)
      expect(reducer(undefined, { type: 'add', payload: 1 })).toBe(1)
      expect(reducer(undefined, { type: 'add', payload: 2 })).toBe(2)
      expect(reducer(undefined, { type: 'sub', payload: 2 })).toBe(-2)
      expect(reducer(undefined, { type: 'sub', payload: 1 })).toBe(-1)
      expect(reducer(1000, { type: 'sub', payload: 1 })).toBe(999)
      expect(reducer(1000, { type: 'add', payload: 1000 })).toBe(2000)
    })
    it('correctly handles a falsy state', () => {
      expect(reducer(false, { type: 'test' })).toBe(false)
    })
  })
})
