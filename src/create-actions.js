import createAction from './create-action'
import prefixType from './prefix-type'
import { SLICE_DELIMITER } from './constants'

export default (baseType, ...args) => {
  let handlers = []
  args.forEach(arg => {
    let argType = typeof arg
    if (argType === 'string') {
      return handlers.push([arg])
    }
    if (argType !== 'object' || arg instanceof Array) {
      return
    }
    Object.keys(arg).forEach(key => handlers.push([key, arg[key]]))
  })
  let create = prefixType(baseType, createAction, SLICE_DELIMITER)

  return handlers.map(handler => create(...handler))
}
