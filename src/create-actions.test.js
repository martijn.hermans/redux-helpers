import createActions from './create-actions'
import { SLICE_DELIMITER } from './constants'

describe('createActions()', () => {
  let actionCreators = createActions()
  it('returns an array', () => {
    expect(Array.isArray(actionCreators)).toBeTruthy()
  })
  it('returns an empty array', () => {
    expect(actionCreators.length).toBe(0)
  })
})
describe('createActions(slice, type)', () => {
  const slice = 'slice'
  const type = 'type'
  let actionCreators = createActions(slice, type)

  it('returns an array', () => {
    expect(Array.isArray(actionCreators)).toBeTruthy()
  })
  it('returns an array with 1 actionCreator', () => {
    expect(actionCreators.length).toBe(1)
  })
  it(`returns an array with an actionCreator of type "${slice}${SLICE_DELIMITER}${type}"`, () => {
    expect(String(actionCreators[0])).toBe(`${slice}${SLICE_DELIMITER}${type}`)
  })
})
describe('createActions(slice, type1, type2)', () => {
  const slice = 'slice'
  const type1 = 'type1'
  const type2 = 'type2'
  let actionCreators = createActions(slice, type1, type2)

  it('returns an array', () => {
    expect(Array.isArray(actionCreators)).toBeTruthy()
  })
  it('returns an array with 2 actionCreators', () => {
    expect(actionCreators.length).toBe(2)
  })
  it(`returns an array with an actionCreators[0] of type "${slice}${SLICE_DELIMITER}${type1}"`, () => {
    expect(String(actionCreators[0])).toBe(`${slice}${SLICE_DELIMITER}${type1}`)
  })
  it(`returns an array with an actionCreators[1] of type "${slice}${SLICE_DELIMITER}${type2}"`, () => {
    expect(String(actionCreators[1])).toBe(`${slice}${SLICE_DELIMITER}${type2}`)
  })
})
describe('createActions(slice, actionMap)', () => {
  const slice = 'slice'
  const actionMap = {
    test1: () => ({}),
    test2: () => ({
      key: 'val'
    }),
    test3: (val) => ({
      key: val
    })
  }
  const types = Object.keys(actionMap)
  let actionCreators = createActions(slice, actionMap)
  const [
    actionCreator1,
    actionCreator2,
    actionCreator3
  ] = actionCreators
  it('returns an array', () => {
    expect(Array.isArray(actionCreators)).toBeTruthy()
  })
  it(`returns an array with ${types.length} actionCreators`, () => {
    expect(actionCreators.length).toBe(types.length)
  })
  types.forEach((type, idx) => {
    it(`returns an array with an actionCreators[${idx}] of type "${slice}${SLICE_DELIMITER}${type}"`, () => {
      expect(String(actionCreators[idx])).toBe(`${slice}${SLICE_DELIMITER}${type}`)
    })
  })
  it('actionCreator1() returns empty action', () => {
    expect(actionCreator1()).toEqual({
      type: `${slice}${SLICE_DELIMITER}${types[0]}`
    })
  })
  it('actionCreator2() returns action with key: "val"', () => {
    expect(actionCreator2()).toEqual({
      type: `${slice}${SLICE_DELIMITER}${types[1]}`,
      key: 'val'
    })
  })
  it('actionCreator3() returns action with key: undefined', () => {
    expect(actionCreator3()).toEqual({
      type: `${slice}${SLICE_DELIMITER}${types[2]}`,
      key: undefined
    })
  })
  it('actionCreator3("test") returns action with key: "test"', () => {
    expect(actionCreator3('test')).toEqual({
      type: `${slice}${SLICE_DELIMITER}${types[2]}`,
      key: 'test'
    })
  })
})

describe('createActions(slice, unsupportedTypes)', () => {
  const slice = 'slice'
  it('createActions(slice, number) returns an empty array', () => {
    let actionCreators = createActions(slice, 123)
    expect(Array.isArray(actionCreators)).toBeTruthy()
    expect(actionCreators.length).toBe(0)
  })
  it('createActions(slice, array) returns an empty array', () => {
    let actionCreators = createActions(slice, [123, 456])
    expect(Array.isArray(actionCreators)).toBeTruthy()
    expect(actionCreators.length).toBe(0)
  })
  it('skips unsupported formats', () => {
    let actionCreators = createActions(slice, 'test1', 1234, 'test2', [], 'test3', { 'test4': (val) => ({ val }) })
    const [
      actionCreator1,
      actionCreator2,
      actionCreator3,
      actionCreator4
    ] = actionCreators
    expect(Array.isArray(actionCreators)).toBeTruthy()
    expect(actionCreators.length).toBe(4)
    expect(String(actionCreator1)).toBe(`${slice}${SLICE_DELIMITER}test1`)
    expect(String(actionCreator2)).toBe(`${slice}${SLICE_DELIMITER}test2`)
    expect(String(actionCreator3)).toBe(`${slice}${SLICE_DELIMITER}test3`)
    expect(String(actionCreator4)).toBe(`${slice}${SLICE_DELIMITER}test4`)
    expect(actionCreator4()).toEqual({
      type: `${slice}${SLICE_DELIMITER}test4`,
      val: undefined
    })
    expect(actionCreator4(1234)).toEqual({
      type: `${slice}${SLICE_DELIMITER}test4`,
      val: 1234
    })
  })
})
